package com.empoweragile.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.empoweragile.refactoringdojo.TemplateProcessor;

public class TemplateTest {

	private TemplateProcessor templateProcessor;
	
	@Before 
	public void initialize() {
		templateProcessor = new TemplateProcessor();
	}

	@SuppressWarnings("deprecation")
	@Test (expected=IndexOutOfBoundsException.class)
	public void testExceptionCodeMissing() {
		templateProcessor.process("algo", "algo2", "halgo3");
		
	}

	@SuppressWarnings("deprecation")
	@Test (expected=IndexOutOfBoundsException.class)
	public void testExceptionAltCodeMissing() {
		templateProcessor.process("algo%CODE%algox", "algo2", "halgo3");
		
	}
	
	@SuppressWarnings("deprecation")
	@Test (expected=StringIndexOutOfBoundsException.class)
	public void testExceptionCodeMinimumLength() {
		templateProcessor.process("algo%CODE%algox%ALTCODE%fin", "algo2", "halgo3");
		
	}
	
	@SuppressWarnings("deprecation")
	@Test 
	public void testHappyPath() {
		String result = templateProcessor.process("algo%CODE%algox%ALTCODE%fin", "12345678", "halgo3");
		assertEquals("algo12345678algox12345-678fin", result);
	}
}
