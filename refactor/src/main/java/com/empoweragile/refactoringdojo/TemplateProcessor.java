package com.empoweragile.refactoringdojo;

public class TemplateProcessor {

	private static final String DELIMITER = "-";
	private static final String ALTCODE = "%ALTCODE%";
	private static final String CODE = "%CODE%";

	@Deprecated
	public String process(String template, String reqId, String code) {
		return generate(template, reqId);

	}

	private String generate(String template, String code) {
		template = template.replace(CODE, code);
		String altcode = formatCode(code);
		template = template.replace(ALTCODE, altcode);
		return template;
	}

	private String formatCode(String code) {
		return code.substring(0, 5) + DELIMITER + code.substring(5, 8);
	}

}
